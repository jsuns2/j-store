package kr.co.jstore.web.module.member.vo.req;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginReqVO implements Serializable {

    private String memberEmail;
    private String memberPassword;

}
