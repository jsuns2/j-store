package kr.co.jstore.web.module.core.wrapper;

import kr.co.jstore.web.module.core.type.ResultCode;
import lombok.Data;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


@Data
public abstract class RestResponseBase {
	protected ResultCode resultCode = ResultCode.OK;
	protected String message;
	protected Date responseAt = DateTime.now().toDate();
	protected Set<Map<String,String>> validationErrorSet = new HashSet<>();

	public abstract boolean hasData();
}
