package kr.co.jstore.web.module.core.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
	OK("ok"),
	NO("no"),
	ERROR("error"),
	INFO("info")
	;

	private String value;
}
