package kr.co.jstore.web.module.core.common.controller;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BaseController {

    protected final static String DEFAULT_SUFFIX = ".default";
    protected final static String EMPTY_SUFFIX = ".empty";
}