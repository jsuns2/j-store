package kr.co.jstore.web.module.main.controller;

import kr.co.jstore.web.module.core.common.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j
@Controller
public class MainController extends BaseController {

    public static final String MAIN = "/";

    public static final String INDEX = MAIN + "index";


    @GetMapping({MAIN, INDEX})
    public String index(){
        return INDEX + EMPTY_SUFFIX;
    }
}