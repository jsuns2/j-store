package kr.co.jstore.web.module.core.wrapper;

import lombok.Data;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class RestListRequest<T> implements Serializable {

	private String lang;
	private Date requestAt;

	@Valid
	private List<T> data;

}
