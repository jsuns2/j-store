package kr.co.jstore.web.module.member.controller;

import kr.co.jstore.web.module.core.wrapper.RestEmptyResponse;
import kr.co.jstore.web.module.member.service.MemberService;
import kr.co.jstore.web.module.member.vo.req.LoginReqVO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MemberActionController {
    private final MemberService memberService;

    @PostMapping(MemberController.LOGIN)
    public RestEmptyResponse loginProc(LoginReqVO reqVO){
        return memberService.loginProc(reqVO);
    }
}
