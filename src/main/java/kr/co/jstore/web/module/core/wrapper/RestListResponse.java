package kr.co.jstore.web.module.core.wrapper;

import kr.co.jstore.web.module.core.type.ResultCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class RestListResponse<T extends Serializable> extends RestResponseBase implements Serializable {

	public static <T extends Serializable> RestListResponse<T> create(T clazz) {
		return new RestListResponse().add(clazz);
	}
	public static <T extends Serializable> RestListResponse<T> create(T... clazz) {
		return new RestListResponse().add(clazz);
	}
	public static <T extends Serializable> RestListResponse<T> create(List<T> clazz) {
		return new RestListResponse().add(clazz);
	}

	public RestListResponse success(){
		this.resultCode(ResultCode.OK);
		this.message("Success");

		long listCount = 0l;
		if(this.data != null && !this.data.isEmpty()){
			listCount = Long.parseLong(String.valueOf(this.data.size()));
		}
		this.listCount(listCount);
		return this;
	}

	public RestListResponse fail(){
		this.resultCode(ResultCode.ERROR);
		return this;
	}

	public RestListResponse fail(String failMsg){
		this.resultCode(ResultCode.ERROR);
		this.message(failMsg == null || failMsg.equals("") ? "Fail" : failMsg);
		return this;
	}

	public RestListResponse responseAt(Date dateTime){
		this.responseAt = dateTime;
		return this;
	}

	public RestListResponse resultCode(ResultCode resultCode){
		this.resultCode = resultCode;
		return this;
	}

	public RestListResponse message(String message){
		this.message = message;
		return this;
	}

	private List<T> data = new ArrayList<>();
	public RestListResponse<T> add(T t) {
		if (t == null) {
			return this;
		}
		data.add(t);
		return this;
	}

	public RestListResponse<T> add(T... ts) {
		if (ts == null || ts.length == 0) {
			return this;
		}
		for (T t : ts) {
			add(t);
		}
		return this;
	}

	public RestListResponse<T> add(List<T> list) {
		if (list == null) {
			return this;
		}
		data.addAll(list);
		return this;
	}

	private Long listCount;
	public RestListResponse<T> listCount(Long listCount) {
		this.listCount = listCount;
		if(this.listCount == null) {
			this.listCount = 0L;
		}
		return this;
	}

	@Override
	public boolean hasData() {
		return data!=null && !data.isEmpty();
	}

}
