package kr.co.jstore.web.module.core.wrapper;

import lombok.Data;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.Date;

@Data
public class RestRequest<T> implements Serializable {

	private String lang;
	private Date requestAt;

	@Valid
	private T data;

}
