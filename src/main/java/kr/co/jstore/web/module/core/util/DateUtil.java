package my.project.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	private static String getStringDate(Date date, String pattern) {
		SimpleDateFormat sdf = null;
		try {
			sdf = new SimpleDateFormat(pattern, Locale.getDefault());
		} catch (NullPointerException | IllegalArgumentException e) {
			sdf = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
		}
		return sdf.format(date);
	}

	/**
	 * 현재 날짜 반환
	 * @param pattern
	 * @return
	 */
	public static String getToday(String pattern) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return getStringDate(cal.getTime(), pattern);
	}

	/**
	 * 날짜 구분자 추가
	 * @param strDate
	 * @param delimiter
	 * @return
	 */
	public static String addDelimiter(String strDate, String delimiter) {
		if(strDate == null || strDate.equals("") || strDate.length() != 8) {
			throw new IllegalArgumentException();
		}
		return strDate.substring(0,4) + delimiter + strDate.substring(4,6) + delimiter + strDate.substring(6,8);
	}

	/**
	 * 날짜 구분자 삭제
	 * @param strDate
	 * @return
	 */
	public static String removeDelimiter(String strDate) {
		if(strDate == null || strDate.equals("") || strDate.length() != 10) {
			throw new IllegalArgumentException();
		}
		return strDate.substring(0,4) + strDate.substring(5,7) + strDate.substring(8,10);
	}

	/**
	 * 날짜 구분자 변경
	 * @param strDate
	 * @param delimiter
	 * @return
	 */
	public static String replaceDelimiter(String strDate, String delimiter) {
		if(strDate == null || strDate.equals("") || strDate.length() != 10) {
			throw new IllegalArgumentException();
		}
		return addDelimiter(removeDelimiter(strDate), delimiter);
	}

	/**
	 * sDate pattern 변경 (inputPattern -> outputPattern으로 변경)
	 * @param sDate
	 * @param inputPattern
	 * @param outputPattern
	 * @return
	 */
	public static String replacePattern(String sDate, String inputPattern, String outputPattern) {
		try {
			if(sDate.length() != inputPattern.length()) {
				throw new IllegalArgumentException();
			}
			SimpleDateFormat sdf1 = new SimpleDateFormat(inputPattern, Locale.getDefault());
			SimpleDateFormat sdf2 = new SimpleDateFormat(outputPattern, Locale.getDefault());
			Calendar cal = Calendar.getInstance();
			cal.setTime(sdf1.parse(sDate));
			return sdf2.format(cal.getTime());
		}catch(NullPointerException | IllegalArgumentException | ParseException e) {
			System.out.println("문제 발생!!");
			return sDate;
		}
	}

	/**
	 * sDate Pattern 변경(yyyyMMdd -> outputPattern으로 변경)
	 * @param sDate
	 * @param outputPattern
	 * @return
	 */
	public static String replacePattern(String sDate, String outputPattern) {
		return replacePattern(sDate, "yyyyMMdd", outputPattern);
	}

	/**
	 * strDate에서 year, month, day만큼 더하기
	 * @param strDate
	 * @param year
	 * @param month
	 * @param day
	 * @param inputPattern
	 * @param outputPattern
	 * @return
	 */
	private static String addYearMonthDay(String strDate, int year, int month, int day, String inputPattern, String outputPattern) {
		try {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(inputPattern, Locale.getDefault());
			cal.setTime(sdf.parse(strDate));
			if(year != 0) {
				cal.add(Calendar.YEAR, year);
			}
			if(month != 0) {
				cal.add(Calendar.MONTH, month);
			}
			if(day != 0) {
				cal.add(Calendar.DATE, day);
			}
			return getStringDate(cal.getTime(), outputPattern);
		}catch(ParseException e) {
			return strDate;
		}
	}

	/**
	 * 현재 일자에서 year, month, day만큼 더하기
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 */
	public static String addYearMonthDay(int year, int month, int day) {
		return addYearMonthDay(new SimpleDateFormat("yyyyMMdd").format(new Date()), year, month, day, "yyyyMMdd", "yyyyMMdd");
	}

	/**
	 * strDate(yyyyMMdd)에서 year, month, day만큼 더하기
	 * @param strDate
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 */
	public static String addYearMonthDay(String strDate, int year, int month, int day) {
		return addYearMonthDay(strDate, year, month, day, "yyyyMMdd", "yyyyMMdd");
	}

	/**
	 * 현재 일자에서 year만큼
	 * @param year
	 * @return
	 */
	public static String addYear(int year) {
		Calendar cal = Calendar.getInstance();
		return addYear(new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(cal.getTime()), year, "yyyyMMdd");
	}

	/**
	 * strDate(yyyyMMdd)에서 year만큼 더하기
	 * @param strDate
	 * @param year
	 * @return
	 */
	public static String addYear(String strDate, int year) {
		return addYear(strDate, year, "yyyyMMdd");
	}

	/**
	 * strDate(inputPattern)에서 year만큼 더하기 
	 * @param strDate
	 * @param year
	 * @param inputPattern
	 * @return
	 */
	public static String addYear(String strDate, int year, String inputPattern) {
		return addYear(strDate, year, inputPattern, null);
	}

	/**
	 * strDate(inputPattern)에서 year만큼 더하기
	 * @param strDate
	 * @param year
	 * @param inputPattern
	 * @param outputPattern
	 * @return
	 */
	public static String addYear(String strDate, int year, String inputPattern, String outputPattern) {
		try {
			if(strDate.length() != inputPattern.length()) {
				throw new Exception();
			}
		}catch(Exception e) {
			System.out.println("입력받은 일자와 inputPattern형식이 같지 않습니다. strDate : " + strDate + ", inputPattern : " + inputPattern);
			return strDate;
		}

		if(outputPattern == null) {
			outputPattern = inputPattern;
		}	
		return addYearMonthDay(strDate, year, 0, 0, inputPattern, outputPattern);
	}

	/**
	 * 현재 일자에서 month만큼 더하기
	 * @param month
	 * @return
	 */
	public static String addMonth(int month) {
		Calendar cal = Calendar.getInstance();
		return addMonth(new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(cal.getTime()), month, "yyyyMMdd");
	}

	/**
	 * strDate(yyyyMMdd)에서 month만큼 더하기 
	 * @param strDate
	 * @param month
	 * @return
	 */
	public static String addMonth(String strDate, int month) {
		return addMonth(strDate, month, "yyyyMMdd");
	}

	/**
	 * strDate(inputPattern)에서 month만큼 더하기 
	 * @param strDate
	 * @param month
	 * @param inputPattern
	 * @return
	 */
	public static String addMonth(String strDate, int month, String inputPattern) {
		return addMonth(strDate, month, inputPattern, null);
	}

	/**
	 * strDate(inputPattern)에서 month만큼 더하기
	 * @param strDate
	 * @param month
	 * @param inputPattern
	 * @param outputPattern
	 * @return
	 */
	public static String addMonth(String strDate, int month, String inputPattern, String outputPattern) {
		try {
			if(strDate.length() != inputPattern.length()) {
				throw new Exception();
			}
		}catch(Exception e) {
			System.out.println("입력받은 일자와 inputPattern형식이 같지 않습니다. strDate : " + strDate + ", inputPattern : " + inputPattern);
			return strDate;
		}

		if(outputPattern == null) {
			outputPattern = inputPattern;
		}	
		return addYearMonthDay(strDate, 0, month, 0, inputPattern, outputPattern);
	}

	/**
	 * 현재 일자에서 day만큼 더하기
	 * @param day
	 * @return
	 */
	public static String addDay(int day) {
		Calendar cal = Calendar.getInstance();
		return addDay(new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(cal.getTime()), day, "yyyyMMdd");
	}

	/**
	 * strDate(yyyyMMdd)에서 day만큼 더하기 
	 * @param strDate
	 * @param day
	 * @return
	 */
	public static String addDay(String strDate, int day) {
		return addDay(strDate, day, "yyyyMMdd");
	}

	/**
	 * strDate(inputPattern)에서 day만큼 더하기 
	 * @param strDate
	 * @param day
	 * @param inputPattern
	 * @return
	 */
	public static String addDay(String strDate, int day, String inputPattern) {
		return addDay(strDate, day, inputPattern, null);
	}

	/**
	 * strDate(inputPattern)에서 day만큼 더하기
	 * @param strDate
	 * @param day
	 * @param inputPattern
	 * @param outputPattern
	 * @return
	 */
	public static String addDay(String strDate, int day, String inputPattern, String outputPattern) {
		try {
			if(strDate.length() != inputPattern.length()) {
				throw new Exception();
			}
		}catch(Exception e) {
			System.out.println("입력받은 일자와 inputPattern형식이 같지 않습니다. strDate : " + strDate + ", inputPattern : " + inputPattern);
			return strDate;
		}

		if(outputPattern == null) {
			outputPattern = inputPattern;
		}	
		return addYearMonthDay(strDate, 0, 0, day, inputPattern, outputPattern);
	}
	
	/**
	 *
	 * @param inputYear : 기준 년
	 * @param inputMonth : 기준 월
	 * @param prevMonth : 최근 몇개월
	 * @return String[0] : 시작일자(yyyyMMdd), String[1] : 마지막 일자(yyyyMMdd)
	 */
	public static String[] getMonthPrevDate(String inputYear, String inputMonth, int prevMonth) {
		String[] result = new String[2];
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		try {
			// 입력 받은 날짜로 설정
			cal.set(Integer.parseInt(inputYear), (Integer.parseInt(inputMonth)-1), 1);
		} catch (NullPointerException | NumberFormatException e) {
			e.printStackTrace();
			// NullPointer, NumberFormat Exception 발생시 현재 달에 첫날과 마지막날 반환
			result[0] = year + String.valueOf(month < 10 ? "0" + month : month) + "01";
			result[1] = year + String.valueOf(month < 10 ? "0" + month : month)
					+ String.valueOf(lastDay < 10 ? "0" + lastDay : lastDay);
			return result;
		}
		year = cal.get(Calendar.YEAR);
		month = cal.get(Calendar.MONTH) + 1;
		// 이번달 마지막날 찾기
		lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		result[1] = year + String.valueOf(month < 10 ? "0" + month : month) + String.valueOf(lastDay < 10 ? "0" + lastDay : lastDay);

		// prevMonth달 설정
		cal.set(year, month-prevMonth-1, 1);
		year = cal.get(Calendar.YEAR);
		month = cal.get(Calendar.MONTH) + 1;
		result[0] = year + String.valueOf(month < 10 ? "0" + month : month) + "01";
		return result;
	}

	/**
	 * 두 날짜 차이 sDate2 - sDate2
	 * @param sDate1
	 * @param sDate2
	 * @param pattern
	 * @return 일 수
	 */
	public static int getDaysDiff(String sDate1, String sDate2, String pattern) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
			Date date1 = sdf.parse(sDate1);
			Date date2 = sdf.parse(sDate2);
			
			if(date1 != null && date2 != null) {
				int days1 = (int)((date1.getTime()/3600000)/24);
				int days2 = (int)((date2.getTime()/3600000)/24);
				return days2 - days1;
			}
			return 0;
			
		}catch(ParseException e) {
			e.printStackTrace();
			System.out.println("date포맷 parsing중 문제 발생");
			return 0;
		}
	}
	
	/**
	 * 두 날짜 차이 sDate2 - sDate1
	 * @param sDate1
	 * @param sDate2
	 * @return 일 수
	 */
	public static int getDaysDiff(String sDate1, String sDate2) {
		return getDaysDiff(sDate1, sDate2, "yyyyMMdd");
	}
	
	/**
	 * 두 날짜 차이 sDate2 - sDate1
	 * @param sDate1
	 * @param sDate2
	 * @return 월 수
	 */
	public static int getMonthDiff(String sDate1, String sDate2) {
		try {
			
			int startYear = Integer.parseInt(sDate1.substring(0,4));
			int startMonth = Integer.parseInt(sDate1.substring(4,6));
			
			int endYear = Integer.parseInt(sDate2.substring(0,4));
			int endMonth = Integer.parseInt(sDate2.substring(4,6));
			
			int result = 0;
			result = (endYear - startYear) * 12;
			result += endMonth - startMonth;
			
			return result;
		}catch(NumberFormatException e) {
			System.out.println("parsing 중 문제 발생");
			return -1;
		}
	}
	
	/**
	 * 해당 년 월 마지막 일자
	 * @param sDate
	 * @param fullFlag
	 * @return
	 */
	public static String getMonthLastDay(String sDate, boolean fullFlag) {
		return getMonthLastDay(sDate, "yyyyMM", fullFlag);
	}
	
	/**
	 * 해당 년월 마지막 일자
	 * @param sDate
	 * @param pattern
	 * @param fullFlag
	 * @return
	 */
	public static String getMonthLastDay(String sDate, String pattern, boolean fullFlag) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
			return getMonthLastDay(sdf.parse(sDate), fullFlag);
		}catch(ParseException e) {
			System.out.println("잘못된 Date형식입니다. sDate : " + sDate);
			return sDate;
		}
	}
	
	/**
	 * 해당 년 월 마지막 일자
	 * @param sDate
	 * @param fullFlag true : 20200730, false : 30
	 * @return
	 */
	private static String getMonthLastDay(Date sDate, boolean fullFlag) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(sDate);
		
		String lastDay = String.valueOf(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		if(fullFlag) {
			return String.valueOf(cal.get(Calendar.YEAR)) + String.valueOf((cal.get(Calendar.MONTH)+1) > 9 ? cal.get(Calendar.MONTH)+1 : "0" + (cal.get(Calendar.MONTH)+1)) + lastDay;
		}else {
			return lastDay;
		}
	}
	
}
