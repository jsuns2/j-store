package kr.co.jstore.web.module.member.controller;

import kr.co.jstore.web.module.core.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MemberController extends BaseController {

    public static final String MAIN = "/member";
    public static final String LOGIN = MAIN + "/login";
    public static final String JOIN = MAIN + "/join";

    @GetMapping(LOGIN)
    public String login(){
        return LOGIN + DEFAULT_SUFFIX;
    }

    @GetMapping(JOIN)
    public String join(){
        return JOIN + DEFAULT_SUFFIX;
    }
}
