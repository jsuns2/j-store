package kr.co.jstore.web.module.member.dao;

import kr.co.jstore.web.module.member.vo.req.LoginReqVO;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberMapper {

    Integer selectMemberCheck(LoginReqVO reqVO);
}
