package kr.co.jstore.web.module.core.wrapper;

import kr.co.jstore.web.module.core.type.ResultCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;


@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class RestSingleResponse<T extends Serializable> extends RestResponseBase implements Serializable {

	public static <T extends Serializable> RestSingleResponse<T> create() {
		return new RestSingleResponse();
	}
	public static <T extends Serializable> RestSingleResponse<T> create(T clazz) {
		return new RestSingleResponse().add(clazz);
	}

	public RestSingleResponse success(){
		this.resultCode(ResultCode.OK);
		this.message("Success");
		return this;
	}

	public RestSingleResponse fail(){
		this.resultCode(ResultCode.ERROR);
		return this;
	}

	public RestSingleResponse fail(String failMsg) {
		this.resultCode(ResultCode.ERROR);
		this.message(failMsg == null || failMsg.equals("") ? "Fail" : failMsg);
		return this;
	}

	public RestSingleResponse responseAt(Date dateTime){
		this.responseAt = dateTime;
		return this;
	}

	public RestSingleResponse resultCode(ResultCode resultCode){
		this.resultCode = resultCode;
		return this;
	}

	public RestSingleResponse message(String message){
		this.message = message;
		return this;
	}

	public RestSingleResponse error(Collection<Map<String,String>> errors){
		validationErrorSet.addAll(errors);
		return this;
	}

	public RestSingleResponse error(Map<String,String> ... errors){
		for(Map<String,String> objectError : errors){
			validationErrorSet.add(objectError);
		}
		return this;
	}

	private T data;
	public RestSingleResponse<T> add(T data) {
		if (data == null) {
			return this;
		}
		this.data = data;
		return this;
	}

	@Override
	public boolean hasData() {
		return data!=null;
	}
}
