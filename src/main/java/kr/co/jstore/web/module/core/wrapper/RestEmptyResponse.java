package kr.co.jstore.web.module.core.wrapper;

import kr.co.jstore.web.module.core.type.ResultCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class RestEmptyResponse extends RestResponseBase implements Serializable {

	public static RestEmptyResponse create() {
		return new RestEmptyResponse();
	}

	public RestEmptyResponse success(){
		this.resultCode(ResultCode.OK);
		this.message("Success");
		return this;
	}

	public RestEmptyResponse fail(){
		this.resultCode(ResultCode.ERROR);
		return this;
	}

	public RestEmptyResponse fail(String failMsg){
		this.resultCode(ResultCode.ERROR);
		this.message(failMsg == null || failMsg.equals("") ? "Fail" : failMsg);
		return this;
	}

	public RestEmptyResponse responseAt(Date dateTime){
		this.responseAt = dateTime;
		return this;
	}

	public RestEmptyResponse resultCode(ResultCode resultCode){
		this.resultCode = resultCode;
		return this;
	}

	public RestEmptyResponse message(String message){
		this.message = message;
		return this;
	}

	public RestEmptyResponse error(Collection<Map<String,String>> errors){
		validationErrorSet.addAll(errors);
		return this;
	}

	public RestEmptyResponse error(Map<String,String>... errors){
		for(Map<String,String> objectError : errors){
			validationErrorSet.add(objectError);
		}
		return this;
	}

	@Override
	public boolean hasData() {
		return false;
	}
}
