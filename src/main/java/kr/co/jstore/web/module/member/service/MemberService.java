package kr.co.jstore.web.module.member.service;

import kr.co.jstore.web.module.core.wrapper.RestEmptyResponse;
import kr.co.jstore.web.module.member.dao.MemberMapper;
import kr.co.jstore.web.module.member.vo.req.LoginReqVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberMapper memberMapper;

    public RestEmptyResponse loginProc(LoginReqVO reqVO) {
        int result = memberMapper.selectMemberCheck(reqVO);
        if(result > 0) {
            return RestEmptyResponse.create().success();
        }
        return RestEmptyResponse.create().fail();
    }
}
