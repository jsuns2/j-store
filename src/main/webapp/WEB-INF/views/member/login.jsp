<%@ page import="kr.co.jstore.web.module.member.controller.MemberController" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">
    <div class="row" style="margin: 10% 30% 0 30%; width: 400px;">
        <h1 class="text-center">
            J-STORE
        </h1>
        <form class="form-horizontal">
            <div style="margin-left: 5%">
                <div class="form-group">
                    <label for="memberEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="memberEmail" id="memberEmail" placeholder="Email" style="width: 80%">
                    </div>
                </div>
                <div class="form-group">
                    <label for="memberPassword" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="memberPassword" id="memberPassword" placeholder="password" style="width: 80%">
                    </div>
                </div>
            </div>
            <div style="width: 100%; margin-left: 10%" >
                <div style="margin-bottom : 1%;">
                    <input type="button" class="btn btn-primary btn-lg btn-block" id="loginBtn" value="Log in" style="width: 300px; height: 45px;" >
                </div>
                <div style="margin-bottom : 1%;">
                    <input type="button" class="btn btn-info btn-lg btn-block" id="registerBtn" value="Sing in"  style="width: 300px; height: 45px;">
                </div>
                <div>
                    <img src="/resources/image/login/kakao_login_btn.png" alt="카카오 로그인 버튼" id="kakaoLoginBtn" style="cursor: pointer;">
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    window.onload = function(){
        let loginBtn = document.getElementById('loginBtn');
        loginBtn.onclick = function(){
            // javascript ajax 호출
            let xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function(){
                if(xhr.readyState === 4) {
                    // 0 = 초기화 되지 않음, open 메소드 호출 되지 않음
                    // 1 = open 메소드 호출, send 메소드 호출 되지 않음
                    // 2 = 송신 완료, send 메소드 호출. 요청 시작
                    // 3 = 수신 중, 서버가 응답을 보내는 중임
                    // 4 = 통신 완료
                    if(xhr.status === 200) { // 통신 성공
                        console.log(xhr.responseText);
                    }
                } else {
                    alert("error : " + xhr.status);
                }
            };
            xhr.open('post','<%=MemberController.LOGIN%>');
            xhr.setRequestHeader('Content-Type', "application/x-www-form-urlencoded; charset=UTF-8");
            xhr.send();
        }

        let registerBtn = document.getElementById('registerBtn');
        registerBtn.onclick = function(){
            // 회원 가입 페이지 이동
        }

        let kakaoLoginBtn = document.getElementById('kakaoLoginBtn');

        kakaoLoginBtn.onclick = function(){
            // 카카오 로그인 버튼 클릭 이벤트
            alert(1);
        }
    }
</script>