<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <tiles:insertAttribute name ="header"/>
</head>
<body>
    <tiles:insertAttribute name ="content"/>

    <footer id="footer">
        <tiles:insertAttribute name ="footer"/>
    </footer>
</body>
</html>
