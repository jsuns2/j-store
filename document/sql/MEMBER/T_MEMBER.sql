CREATE TABLE T_MEMBER
(
    member_seq int auto_increment primary key comment '회원 SEQ'
	, member_type char(1) not null comment '회원 가입 TYPE(J : 일반, K : 카카오, N : 네이버)'
	, member_name varchar(10) not null comment '회원 이름'
	, member_email varchar(50) not null comment '회원 이메일'
	, member_password varchar(40) not null comment '회원 비밀번호'
	, member_phonenumber varchar(11) comment '회원 전화번호'
	, member_zipcode varchar(5) comment '우편번호'
	, member_addr1 varchar(50) comment '주소1'
	, member_addr2 varchar(50) comment '주소2'
	, member_sleep_yn char(1) not null default 'N' comment '휴면 계정 여부'
	, reg_dt timestamp not null default current_timestamp comment '등록일'
	, reg_id varchar(20) comment '등록자'
	, mod_dt timestamp not null default current_timestamp comment '수정일'
	, mod_id varchar(20) comment '수정자'
) comment = '회원 테이블';

