create table t_dormancy_member
(
	dormancy_seq int auto_increment primary key comment '휴면 SEQ'
	, member_seq int not null comment '회원 SEQ'
	, member_sleep_yn char(1) not null comment '휴면 계정 여부'
	, reg_dt timestamp not null default current_timestamp comment '등록일'
	, reg_id varchar(20) comment '등록자'
	, foreign key(member_seq) references t_member(member_seq)
)